package application;

import entities.Estudante;

import java.util.Locale;
import java.util.Scanner;

public class Principal {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        System.out.print("How many rooms will be rented? ");
        int quantidade = sc.nextInt();

        Estudante[] estudante = new Estudante[quantidade];

        for (int i = 0; i < estudante.length; i++) {
            sc.nextLine();
            System.out.printf("Rent #%d: ", i + 1, "\n");
            System.out.println(" ");
            System.out.print("Name: ");
            String nome = sc.nextLine();
            System.out.print("Email: ");
            String email = sc.nextLine();
            System.out.print("Room: ");
            int quarto = sc.nextInt();
            System.out.println(" ");

            estudante[i] = new Estudante(nome, email, quarto);
        }

        // Ordenar o vetor de estudantes usando o algoritmo Selection Sort
        for (int i = 0; i < estudante.length - 1; i++) {
            int indiceMinimo = i;
            for (int j = i + 1; j < estudante.length; j++) {
                if (estudante[j].getNumeroQuarto() < estudante[indiceMinimo].getNumeroQuarto()) {
                    indiceMinimo = j;
                }
            }

            Estudante temp = estudante[i];
            estudante[i] = estudante[indiceMinimo];
            estudante[indiceMinimo] = temp;
        }

        System.out.println("Busy rooms: ");
        for (int i = 0; i < estudante.length; i++) {
                System.out.println(estudante[i]);
        }

    }
}
